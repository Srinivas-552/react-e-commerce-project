const fs = require('fs')
// const fsPromises = require('fs').promises;
const cors = require("cors");

const express = require('express');
const app = express();
const router = express.Router();
app.use(cors());
app.use(express.static('public'));
app.use(express.json({limit: '1mb'}));
const port = 9000


router.get('/', (req, res) => {
  res.sendFile('./products.json', {root: __dirname})
})


router.get('/product/:id', (req,res) =>{
  // console.log(req.params)
  const { id } = req.params || {};

  if(!id) return;

  fs.promises.readFile('./products.json', {  encoding:'utf-8'})
  .then(result => {
    const parseResult = JSON.parse(result);
    const found = parseResult.response.data.find(eachObj => eachObj.id == id);
    if(found != null) {
      return res.send({code:200, status:"Success", data: found});
    }
    else{
      return res.send({code:404, status:"Not Found", data:null});
    }
  })
  .catch(err => console.log(err))
})



router.post('/orders', async(req, res) => {
  // let orderArr = '[]';
  const jsonData = req.body;
  // console.log(jsonData)
  let randomNum = Math.floor(Math.random()*10000) + 1;

  // if(fs.existsSync('./orders.json')){
  //  orderArr = await fs.promises.readFile('./orders.json', 'utf-8')
   
  // }
  let orderArr =await fs.promises.readFile('./orders.json', 'utf-8')
  orderData = (orderArr==='' ? '[]' : orderArr)

  if(jsonData.length>0){
    let data = JSON.stringify([...(JSON.parse(orderData)), {[randomNum]: jsonData}])
    console.log(data)
    fs.promises.writeFile('./orders.json', data)
      .then(result => res.send({status: 'Success'}))
      .catch(err => res.send({status: err}))
  } 
  
  })

router.get('/orders', (req, res) => {
  res.sendFile('./orders.json', {root: __dirname})
})


app.use(router)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})