import React, { Component } from 'react';
import { Routes, Route } from "react-router-dom";
import Header from '../components/Header';
import Home from '../components/Home';
import Cart from '../components/Cart';
import ProductDetails from '../components/ProductDetails';
import MyOrders from '../components/MyOrders';

class AppRouter extends Component {
  render() {
    return (
        <>
        <Header />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/product/:id' element={<ProductDetails />} />
          <Route path='/cart' element={<Cart />} />
          <Route path='/orders' element={<MyOrders />} />
        </Routes>
      </>
    )
  }
}

export default AppRouter