import React, { Component } from 'react';
// import { Routes, Route } from "react-router-dom"
// import Header from './components/Header';
// import Home from './components/Home';
// import ProductDetails from './components/ProductDetails';
import ContextOperation from "./components/Context/contextOperation";
import AppRouter from './AppRouter/router';


class App extends Component {

  render() {
    
    return (
      <>
        <ContextOperation>
          <AppRouter />
        </ContextOperation>
      </>
    )
  }
}

export default App