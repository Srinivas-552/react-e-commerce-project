import React, { Component } from 'react'
import './style.css';
import {Link} from 'react-router-dom';

class Header extends Component {
  render() {
    return (
        <>
            <nav>
              <ul>
                <li><Link to='/'>Home</Link></li>
                <li><Link to='/cart'>Cart</Link></li>
                <li><Link to='/orders'>MyOrders</Link></li>
              </ul>
            </nav>
        </>
    )
  }
}

export default Header;