import React, { Component } from 'react';

const CartContext = React.createContext({
    apiResponse: [],
    orders: [],
    increment: () => {},
    decrement: () => {},
    updateResponse: () => {},
    updateOrderData: () => {}
});


class ContextOperation extends Component {
  constructor(props){
    super(props)
    this.state = {
      apiResponse: [],
      orders: []
    }
  }

  componentDidMount(){
    this.getFetchData()
  }

  getFetchData = async () => {
    const response = await fetch('http://localhost:9000/')
    const fetchedData = await response.json()
    let arrData = fetchedData.response.data
    
    this.setState({
        apiResponse: arrData
      })
  }


  increment = (index) => {
      let updatedResponse = this.state.apiResponse;
      updatedResponse[index].quantity += 1;
      this.setState({
          apiResponse: updatedResponse
      })
  }

  decrement = (index) => {
    let updatedResponse = this.state.apiResponse;
    updatedResponse[index].quantity -= 1;
    this.setState({
        apiResponse: updatedResponse
    })
  }

    updateResponse = (updateResponse) => {
      this.setState({
        apiResponse: updateResponse
      })
    }

    updateOrderData = (updateOrder) => {
      this.setState({
        orders: updateOrder
      })
    }


  render() {
    
    return (
        <CartContext.Provider value={{
            apiResponse: this.state.apiResponse,
            orders: this.state.orders,
            increment: this.increment,
            decrement: this.decrement,
            updateResponse: this.updateResponse,
            updateOrderData: this.updateOrderData
        }}>
            {this.props.children}
        </CartContext.Provider>
    )
  }
}

export default ContextOperation

export {
    CartContext
}