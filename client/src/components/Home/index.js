import React, { Component } from 'react';
import { CartContext } from '../Context/contextOperation';
import Product from './product';
import './style.css';

class Home extends Component {
    

  render() {
    const {apiResponse} = this.context
    
    return (
      <div className='product-container'>
          {apiResponse.map((item, index) => (
              <Product shopData={item} index={index} key={item.id} />
          ))}
          
      </div>
    )
  }
}

Home.contextType = CartContext

export default Home;