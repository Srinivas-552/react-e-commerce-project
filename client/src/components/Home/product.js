import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import { CartContext } from '../Context/contextOperation';
import './style.css';

class Product extends Component {
    
  
    onIncrement = () => {
      let index = this.props.index
      this.context.increment(index)
    }

    onDecrement = () => {
      let index = this.props.index
      this.context.decrement(index)
    }

  render() {
    const {id, price, name, url, quantity} = this.props.shopData

    return (
       <div >
         <div className='item-container'>

            <Link to={`/product/${id}`}  className='link-items' >
                <img src={url} alt={name} className='image'/>
                <div className='info-container'>
                    <h3>{name}</h3>
                    <p className='item-price'><span className='price'>Rs: </span>{price}</p>
                </div>
             </Link>

             <div>
                {quantity === 0 ? 
                (<button className='add-btn' onClick={this.onIncrement}>Add To Cart</button>):
                (
                <div className='click-btns'>
                  <button onClick={this.onDecrement}>-</button>
                  <span className='quantity'>{quantity}</span>
                  <button onClick={this.onIncrement}>+</button>
                </div>
                )}
            </div>
        </div>
    </div>
  
    )
  }
}

Product.contextType = CartContext;

export default Product

