import React, { Component } from 'react';
import withRouter from '../../hoc/withRouter';
// import { useParams } from 'react-router-dom';
import './index.css';

class ProductDetails extends Component {
    constructor(props){
      super(props)
      this.state = {
        productData: {}
      }
    }

    componentDidMount() {
        this.getItemData()
      }
    
    getItemData = async () => {
    const id = this.props.router.params.id;
      // console.log(id)
    
        const response = await fetch(`http://localhost:9000/product/${id}`)
        const {data} = await response.json()
        // console.log(data)
        this.setState({
          productData : data
        })

    }

  render() {
    const {productData} = this.state
    const { price, name, url, quantity} = productData
    // console.log(productData)

    return (
      <div>
        <div className='item-container'>
            <img src={url} alt={name} className='image'/>
            <div className='info-container'>
                <h3>{name}</h3>
                <p className='item-price'><span className='price'>Rs: </span>{price}</p>
            </div>
            <div>
              {quantity===0 ? 
              (<button className='add-btn' >Add To Cart</button>):
              (
              <div className='button-container'>
                <button>-</button>
                <span>{quantity}</span>
                <button>+</button>
              </div>
              )}
            </div>
        </div>
      </div>
    )
  }
}

export default withRouter(ProductDetails);