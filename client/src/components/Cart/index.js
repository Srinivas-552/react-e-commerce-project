import React, { Component } from 'react';
import CartItem from './cartItem';
import { CartContext } from '../Context/contextOperation';
import './style.css';

class Cart extends Component {

    
    updateCartItem = async () => {
        const {apiResponse} = this.context
        const cartItems = apiResponse.filter(curr => curr.quantity>0)
          // let randomNum = Math.floor(Math.random()*10000) + 1;
        // console.log(cartItems)
        
        const updateZeroQuantity = apiResponse.reduce((acc, curr) => {
            acc.push({...curr, quantity:0})
          return acc
        }, [])
        this.context.updateResponse(updateZeroQuantity)

        
        const url = 'http://localhost:9000/orders'
        const options = {
          method: 'POST',
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(cartItems)
        }
        const response = await fetch(url, options)
        const data = await response.json()
        console.log(data)
    }

  render() {
      const {apiResponse} = this.context
    let itemPrice= 0
    let totalPrice=0
    return (
        <div id='product-container'>
          {apiResponse.map((item, index) => {
            itemPrice = item.quantity*item.price
            totalPrice += itemPrice
            
             return <CartItem shopData={item} itemPrice={itemPrice} totalPrice={totalPrice} index={index} key={item.id} />
            })}

          {totalPrice>0 ? (
            <div>
                <h3>Total Price: <span className='price'>Rs </span>{totalPrice}</h3>
                <button className='order-button' onClick={this.updateCartItem}>Place order</button>
            </div>) : <h1>Empty Cart</h1> }
      </div>
    )
  }
}

Cart.contextType = CartContext;

export default Cart

