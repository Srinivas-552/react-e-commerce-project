import React, { Component } from 'react';
import { CartContext } from '../Context/contextOperation';
import './style.css';

export class CartItem extends Component {

    onIncrement = () => {
          let index = this.props.index
          this.context.increment(index)
        }
    
    onDecrement = () => {
      let index = this.props.index
      this.context.decrement(index)
    }

  render() {
    const { name, url, quantity} = this.props.shopData
    // const{itemPrice} = this.props.itemPrice
    // console.log(this.props.totalPrice)
    return (
        <div> 
            {(quantity > 0) ?
            (<div id='item-container'>
                <img src={url} alt={name} id='image'/>
                <div className='info-container'>
                    <h3>{name}</h3>
                    <p className='item-price'><span className='price'>Rs: </span>{this.props.itemPrice}</p>
                </div>

                <div>
                    {(quantity === 0) ? 
                    (<button className='add-btn' onClick={this.onIncrement}>Add To Cart</button>):
                    (  <div className='buttons'>
                            <button className='minus-btn' onClick={this.onDecrement}>-</button>
                            <span className='quantity'>{quantity}</span>
                            <button onClick={this.onIncrement}>+</button>
                        </div>)}
                </div>
            </div>) : "" }
        </div>
      
    )
  }
}

CartItem.contextType = CartContext

export default CartItem