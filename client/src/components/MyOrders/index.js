import React, { Component } from "react";
import { CartContext } from "../Context/contextOperation";
import "./style.css";

class MyOrders extends Component {
  componentDidMount() {
    this.getFetchOrders();
  }

  getFetchOrders = async () => {
    const response = await fetch("http://localhost:9000/orders");
    const orderData = await response.json();
    // console.log(orderData)

    this.context.updateOrderData(orderData);
  };

  render() {
    const { orders } = this.context;
    let itemPrice = 0;
    let totalPrice = 0;
    return (
      <div id="order-container">
        {orders.map(eachItem=>{
            // console.log(eachItem)
            const orderId= Object.keys(eachItem)[0]
            return(<ul id="order-item-container" key={orderId}>
                <li className="order-id">orderID: {orderId}</li>
                {eachItem[orderId].map(item=>{
                    itemPrice = item.quantity*item.price
                    totalPrice += itemPrice
                    return <div className="order-list">
                    <li className="item">{item.name}</li>
                    <li className="item">{item.quantity}</li>
                    <li className="item">x</li>
                    <li className="item"><span className="price">Rs</span> {item.price}</li>
                    <li className="item">=</li>
                    <li className="item"><span className="price">Rs</span> {itemPrice}</li>
                    </div>
                })}
                {/* <li className="order-id">Total Amount: <span className="price">Rs</span> {totalPrice}</li> */}
            </ul>)
        })}
        {totalPrice<0 && <h1>No Orders</h1>}
      </div>
    );
  }
}

MyOrders.contextType = CartContext;

export default MyOrders;
